//Other data structures Maps and Slices
package main

import "log"

/*
func main() {
	var myString string
	var myInt int

	myString = "Hi"
	myInt = 11

	mySecondString := "Another String"

	log.Println(myString, myInt, mySecondString)
}
*/

/*
func main() {
	myMap := make(map[string]string)

	myMap["dog"] = "Samson"
	myMap["other-dog"] = "Cassie"
	myMap["dog"] = "Fido" //Ganti "Samson" jadi "Fido"

	log.Println(myMap["dog"])
	log.Println(myMap["other-dog"])

}
*/

/*
//MAP
type User struct {
	FirstName string
	LastName  string
}

func main() {
	myMap := make(map[string]User)
	me := User{
		FirstName: "Alfian",
		LastName:  "Yuandika",
	}

	myMap["me"] = me

	log.Println(myMap["me"].FirstName, myMap["me"].LastName)

}
*/

//Slice
/*
func main() {
	var mySlice []string
	var mySlice2 []int

	mySlice = append(mySlice, "Alfian")
	mySlice = append(mySlice, "Yuandika")
	mySlice = append(mySlice, "Putra")

	mySlice2 = append(mySlice2, 2)
	mySlice2 = append(mySlice2, 1)
	mySlice2 = append(mySlice2, 3)

	sort.Strings(mySlice)
	sort.Ints(mySlice2)

	log.Println(mySlice)
	log.Println(mySlice2)
}
*/

func main() {
	numbers := []int{5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}           //shorthand
	names := []string{"Satu", "Dua", "Indonesia", "Kucing", "Laptop"} //shorthand

	log.Println(numbers)
	log.Println(numbers[6:11]) //Slice (mulai dari index ke-6 sampai index ke-10) //includes the first element, but excludes the last one

	log.Println(names)
	log.Println(names[1:4]) //Slice (mulai dari index ke-1 sampai index ke-3) //includes the first element, but excludes the last one

}
