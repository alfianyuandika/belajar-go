// Loops and ranging over data
package main

import "log"

/*
func main() {
	for i := 0; i <= 10; i++ {
		log.Println(i)
	}
}
*/

/*
func main() {
	mySlice := []string{"dog", "cat", "horse", "fish", "banana"}

	for _, x := range mySlice {
		log.Println(x)

	}
}
*/

/*
func main() {
	myMap := make(map[string]string)
	myMap["1"] = "dog"
	myMap["2"] = "fish"
	myMap["3"] = "hat"

	for i, x := range myMap {
		log.Println(i, x) //akan print random ex: dog, fish, hat; fish, hat, dog; //map selalu di random. belum tentu sesuai dengan urutan penulisan
	}
}
*/

type User struct {
	FirstName string
	LastName  string
}

func main() {
	var mySlice []User

	u1 := User{
		FirstName: "Alfian",
	}

	u2 := User{
		FirstName: "Yuandika",
	}

	mySlice = append(mySlice, u1)
	mySlice = append(mySlice, u2)
	for i, x := range mySlice {
		log.Println(i, x.FirstName)
	}

}
