// Decision Structures
package main

import "log"

/*
func main() {
	var isTrue bool

	isTrue = false

	if isTrue {
		log.Println("isTrue is", isTrue)
	} else {
		log.Println("isTrue is", isTrue)
	}

}
*/

/*
func main() {
	cat := "cat2"

	if cat == "cat" {
		log.Println("Cat is Cat")
	} else {
		log.Println("Cat is not cat")
	}
}
*/

/*
func main() {
	myNum := 1000
	isTrue := false

	if myNum > 99 && !isTrue {
		log.Println("myNum is greater than 99 isTrue is set to true")
	} else if myNum < 100 && isTrue {
		log.Println("1")
	} else if myNum == 101 || isTrue {
		log.Println("2")
	} else if myNum > 1000 && isTrue == false {
		log.Println("3")
	}
}
*/

func main() {
	myVar := "dog"

	switch myVar {

	case "cat":
		log.Println("cat is set to cat")
	case "dog":
		log.Println("cat is set to dog")
	case "fish":
		log.Println("cat is set to fish")
	default:
		log.Println("cat is something else")
	}
}
