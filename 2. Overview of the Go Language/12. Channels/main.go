//Channels
package main

import (
	"channels/helpers"
	"log"
)

const numPool = 1000

func CalculateValue(intChan chan int) {
	randomNumber := helpers.RandomNumber(numPool)
	intChan <- randomNumber
}
func main() {
	intChan := make(chan int)
	defer close(intChan) //Defer = whatever comes after "defer" execute that as soon as the current function is done

	go CalculateValue(intChan) //Go routines

	num := <-intChan
	log.Println(num)
}
