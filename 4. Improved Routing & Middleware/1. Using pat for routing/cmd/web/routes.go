package main

import (
	"net/http"

	"github.com/bmizerany/pat"
	"gitlab.com/alfianyuandika/go-course/pkg/config"
	"gitlab.com/alfianyuandika/go-course/pkg/handlers"
)

func routes(app *config.AppConfig) http.Handler {
	mux := pat.New()

	mux.Get("/home", http.HandlerFunc(handlers.Repo.Home))
	mux.Get("/about", http.HandlerFunc(handlers.Repo.About))

	return mux
}
